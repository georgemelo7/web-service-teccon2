<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//namespace Endicon\Test\Modulos;

/**
 * Description of ComunicadoTest
 *
 * @author George Tassiano
 */

use App\Modulos\Usuario;
class UsuarioTest extends PHPUnit_Framework_TestCase {

    /**
     * @test
     */
    public function TestToArray() {
        $comunicado = new Usuario('george.melo7@gmail.com', 'george tassiano', 'admin');
        $array = array(
            'login' => 'george.melo7@gmail.com',
            'name' => 'george tassiano',
            'credential' => 'admin'
        );
        
        $this->assertEquals($array, $comunicado->toArray());
    }

}
