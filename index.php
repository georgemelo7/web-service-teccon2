<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Firebase\JWT\JWT;

require 'vendor/autoload.php';

$app = new \Slim\App(['settings' => [
        'displayErrorDetails' => true]]);


$container = $app->getContainer();

$container["jwt"] = function ($container) {
    return new StdClass;
};

$app->add(new \Slim\Middleware\JwtAuthentication([
    "secret" => "teste",
    "path" => "/",
    "passthrough" => "/login",
    "secure" => false,
    "callback" => function ($request, $response, $arguments) use ($container) {
        $container["jwt"] = $arguments["decoded"];
    }
]));

$app->post('/login', function (Request $req, Response $res, $args) {
    $control = new App\Controllers\ControllerLogin;
    $dados = $req->getParsedBody();
    $req->withParsedBody(null);
    if ($control->autentificado($dados)) {
        $payload = [
            "scope" => $control->getPermissao()
        ];
        $secret = "teste";
        $token = JWT::encode($payload, $secret, "HS256");
        $data["id_token"] = $token;
        $data["credential"] = $control->getPermissao();
        $data["email"] = $dados["Login"];
        return $res->withStatus(201)->withHeader("Content-Type", "application/json")->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    } else {
        return $res->withStatus(401);
    }
});

$app->get('/dados/usuarios', function (Request $req, Response $res, $args) {
    if (in_array("admin", $this->jwt->scope)) {
        $control = new App\Controllers\ControllerUsuario();
        return $res->withJson($control->getUsuarios(), 200);
    } else {
        $res->withStatus(401);
    }
});

$app->get('/dados/usuario', function (Request $req, Response $res, $args) {
        $control = new App\Controllers\ControllerUsuario();
        $data = $req->getParsedBody();
        return $res->withJson($control->getUsuario($data), 200);
});

$app->delete('/dados/usuarios', function (Request $req, Response $res, $args) {
    if (in_array("admin", $this->jwt->scope)) {
        $control = new App\Controllers\ControllerUsuario();
        $control->deleteUsuario($req->getParsedBody());
        return $res->withStatus(204);
    } else {
        $res->withStatus(401);
    }
});

$app->put('/dados/usuarios', function (Request $req, Response $res, $args) {
    $control = new App\Controllers\ControllerUsuario();
    $control->putUsuario($req->getParsedBody());
    return $res->withStatus(201);
});

$app->post('/dados/usuarios/novo_usuario', function (Request $req, Response $res, $args) {
    if (in_array("admin", $this->jwt->scope)) {
        $control = new App\Controllers\ControllerUsuario();
        $data = $req->getParsedBody();
        $req->withParsedBody(null);
        return $res->withJson($control->postUsuario($data), 201);
    } else {
        $res->withStatus(401);
    }
});
$app->run();
