-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 17-Jan-2017 às 03:58
-- Versão do servidor: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `teccon`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_data`
--

CREATE TABLE `tb_data` (
  `idTB_Data` int(11) NOT NULL,
  `Value` double NOT NULL,
  `Time` datetime NOT NULL,
  `TB_Sensor_idTB_Sensor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_interrogator`
--

CREATE TABLE `tb_interrogator` (
  `idTB_Interrogator` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `TB_InterrogatorRef_idTB_InterrogatorRef` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_sensor`
--

CREATE TABLE `tb_sensor` (
  `idTB_Sensor` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Channel` int(11) NOT NULL,
  `TB_Interrogator_idTB_Interrogator` int(11) NOT NULL,
  `TB_SensorRef_idTB_SensorRef` int(11) NOT NULL,
  `TB_User_Login` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_sensorref`
--

CREATE TABLE `tb_sensorref` (
  `idTB_SensorRef` int(11) NOT NULL,
  `MaxValue` double NOT NULL,
  `MinValue` double NOT NULL,
  `Precision` double NOT NULL,
  `Serial` varchar(30) DEFAULT NULL,
  `TB_SensorType_idTB_SensorType` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_sensortype`
--

CREATE TABLE `tb_sensortype` (
  `idTB_SensorType` int(11) NOT NULL,
  `Name` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_user`
--

CREATE TABLE `tb_user` (
  `Login` varchar(100) NOT NULL,
  `Password` varchar(300) NOT NULL,
  `Credential` varchar(45) NOT NULL,
  `Name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_user`
--

INSERT INTO `tb_user` (`Login`, `Password`, `Credential`, `Name`) VALUES
('admin2@teste.com', '$2y$10$FQE8rmtR9c5QYR5RsTuANOJ3n7029Ev6.didD6kiSNW4NxW7oafDq', 'normal', 'amanda'),
('admin@teste.com', '$2y$10$uKC2v7YMS4UMNkMHLNomU.NGHXNHx0/pvs1HRgdm9m8T4GQ6FXsDm', 'admin', 'george');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_data`
--
ALTER TABLE `tb_data`
  ADD PRIMARY KEY (`idTB_Data`),
  ADD KEY `fk_TB_Data_TB_Sensor1_idx` (`TB_Sensor_idTB_Sensor`);

--
-- Indexes for table `tb_interrogator`
--
ALTER TABLE `tb_interrogator`
  ADD PRIMARY KEY (`idTB_Interrogator`);

--
-- Indexes for table `tb_sensor`
--
ALTER TABLE `tb_sensor`
  ADD PRIMARY KEY (`idTB_Sensor`),
  ADD KEY `fk_TB_Sensor_TB_Interrogator1_idx` (`TB_Interrogator_idTB_Interrogator`),
  ADD KEY `fk_TB_Sensor_TB_SensorRef1_idx` (`TB_SensorRef_idTB_SensorRef`),
  ADD KEY `fk_TB_Sensor_TB_User1_idx` (`TB_User_Login`);

--
-- Indexes for table `tb_sensorref`
--
ALTER TABLE `tb_sensorref`
  ADD PRIMARY KEY (`idTB_SensorRef`),
  ADD KEY `fk_TB_SensorRef_TB_SensorType1_idx` (`TB_SensorType_idTB_SensorType`);

--
-- Indexes for table `tb_sensortype`
--
ALTER TABLE `tb_sensortype`
  ADD PRIMARY KEY (`idTB_SensorType`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`Login`);

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tb_data`
--
ALTER TABLE `tb_data`
  ADD CONSTRAINT `fk_TB_Data_TB_Sensor1` FOREIGN KEY (`TB_Sensor_idTB_Sensor`) REFERENCES `tb_sensor` (`idTB_Sensor`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_sensor`
--
ALTER TABLE `tb_sensor`
  ADD CONSTRAINT `fk_TB_Sensor_TB_Interrogator1` FOREIGN KEY (`TB_Interrogator_idTB_Interrogator`) REFERENCES `tb_interrogator` (`idTB_Interrogator`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_Sensor_TB_SensorRef1` FOREIGN KEY (`TB_SensorRef_idTB_SensorRef`) REFERENCES `tb_sensorref` (`idTB_SensorRef`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_Sensor_TB_User1` FOREIGN KEY (`TB_User_Login`) REFERENCES `tb_user` (`Login`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_sensorref`
--
ALTER TABLE `tb_sensorref`
  ADD CONSTRAINT `fk_TB_SensorRef_TB_SensorType1` FOREIGN KEY (`TB_SensorType_idTB_SensorType`) REFERENCES `tb_sensortype` (`idTB_SensorType`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
