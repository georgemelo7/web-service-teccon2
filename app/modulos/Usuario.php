<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Usuario
 *
 * @author George Tassiano
 */

class Usuario {
  private $login;
  private $name;
  private $credential;

  public function __construct($login, $name, $credential) {
    $this->login = $login;
    $this->name = $name;
    $this->credential = $credential;
  }

  public function getLogin() {
    return $this->login;
  }

  public function setLogin($login) {
    $this->login = $login;
  }

  public function getName() {
    return $this->name;
  }

  public function setName($name) {
    $this->name = $name;
  }

  public function getCredential() {
    return $this->credential;
  }

  public function setCredential($credential) {
    $this->credential = $credential;
  }

  public function toArray() {
    $json = array(
      'login' => $this->login,
      'name' => $this->name,
      'credential' => $this->credential
    );
    return $json;
  }

}
