<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

class BD {

  private $host = "localhost";
  private $usuario = "root";
  private $senha = "";
  private $nomebd = "teccon";
  private $dbh;
  public $erro = false;
  public $mensagemerro;
  private $stmt;

  public function __construct() {
    $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->nomebd . ';charset=utf8';

    $options = array(
        \PDO::ATTR_PERSISTENT => true,
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
    );

    try {
      $this->dbh = new \PDO($dsn, $this->usuario, $this->senha, $options);
    } catch (PDOException $e) {
      $this->erro = true;
      $this->mensagemerro = $e->getMessage();
    }
  }

  public function close() {
    $this->dbh = null;
  }

  public function query($query) {
    $this->stmt = $this->dbh->prepare($query);
  }

  public function bind($param, $value, $type = null) {
    if (is_null($type)) {
      switch (true) {
        case is_int($value):
          $type = \PDO::PARAM_INT;
          break;
        case is_bool($value):
          $type = \PDO::PARAM_BOOL;
          break;
        case is_null($value):
          $type = \PDO::PARAM_NULL;
          break;
        default:
          $type = \PDO::PARAM_STR;
      }
    }
    $this->stmt->bindValue($param, $value, $type);
  }

  public function execute() {
    return $this->stmt->execute();
  }

  public function resultset() {
    return $this->stmt->fetchAll(\PDO::FETCH_ASSOC);
  }

  public function single() {
    return $this->stmt->fetch(\PDO::FETCH_ASSOC);
  }

  public function rowCount() {
    return $this->stmt->rowCount();
  }

  public function lastInput() {
    return $this->dbh->lastInsertId();
  }

}
