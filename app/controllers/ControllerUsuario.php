<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerUsuario
 *
 * @author ADM
 */
use App\Modulos\BD;
use App\Modulos\Usuario;

class ControllerUsuario {

  public function getUsuario($login) {
    if ($login == NULL) {
      $usu = new Usuario();
      return $usu->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_User WHERE Login = :login";
      $bd->query($sql);
      $bd->bind(':login', $login);
      $bd->execute();
      $row = $bd->single();
      if (!empty($row)) {
        $usu = new Usuario($row["Login"], $row["Name"], $row["Credential"]);
        $usuario = $usu->toArray();
      } else {
        $usuario = null;
      }
      $bd->close();
      return $usuario;
    }
  }

  public function getUsuarios() {
    $bd = new BD();
    $sql = "SELECT * FROM TB_User";
    $bd->query($sql);
    if ($bd->execute()) {
      $usuarios = array();
      while ($row = $bd->single()) {
        $usu = new Usuario($row["Login"], $row["Name"], $row["Credential"]);
        $usuarios[] = $usu->toArray();
      }
    } else {
      $usuarios = null;
    }
    $bd->close();
    return $usuarios;
  }

  public function deleteUsuario($dados) {
    $bd = new BD();
    $sql = "DELETE FROM TB_User WHERE Login = :login";
    $bd->query($sql);
    $bd->bind(':login', $dados["Login"]);
    $bd->execute();
    $bd->close();
  }

  public function postUsuario($dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_User (Login, Password, Name, Credential) VALUES (:login, :password, :name, :credential)";
    $bd->query($sql);
    $bd->bind(':login', $dados["Login"]);
    $bd->bind(':password', password_hash($dados["Password"], PASSWORD_DEFAULT));
    $bd->bind(':name', $dados["Name"]);
    $bd->bind(':credential', $dados["Credential"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putUsuario($dados) {
    $bd = new BD();
    if ($dados["Password"] == NULL) {
      $sql = "UPDATE TB_User SET Login=:login, Name=:name, Credential=:credential WHERE Login = :login";
      $bd->query($sql);
      $bd->bind(':login', $dados["Login"]);
      $bd->bind(':name', $dados["Name"]);
      $bd->bind(':credential', $dados["Credential"]);
      $bd->execute();
      $bd->close();
    } else {
      $sql = "UPDATE TB_User SET Login=:login, Password=:password, Name=:name, Credential=:credential WHERE Login = :login";
      $bd->query($sql);
      $bd->bind(':login', $dados["Login"]);
      $bd->bind(':password', password_hash($dados["Password"], PASSWORD_DEFAULT));
      $bd->bind(':name', $dados["Name"]);
      $bd->bind(':credential', $dados["Credential"]);
      $bd->execute();
      $bd->close();
    }
  }

}
