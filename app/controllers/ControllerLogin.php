<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerLogin
 *
 * @author George Tassiano
 */
use App\Modulos\BD;

class ControllerLogin {

  private $permissao;

  public function __construct() {
    $this->permissao = array();
  }

  public function getPermissao() {
    return $this->permissao;
  }

  public function autentificado($dados) {
    $bd = new BD();
    $sql = "SELECT * FROM TB_User WHERE Login = :login";
    $bd->query($sql);
    $bd->bind(':login', $dados["Login"]);
    $bd->execute();
    $row = $bd->single();
    if (!empty($row)) {
      if (password_verify($dados["Password"], $row["Password"])) {
        $this->permissao[] = $row["Credential"];
        $bd->close();
        return true;
      } else {
        $bd->close();
        return false;
      }
    } else {
      $bd->close();
      return false;
    }
  }

}
